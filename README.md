# EpiHubCodingClub_240408

## Making coding more efficient by writing functions and packages 

There is a natural evolution as you learn more and more about how to express yourself in scripts, 
for example in R to do data analysis, data wrangling or visualizations.

In the accompanying power point presentation, I have tried to highlight some of those steps 
on my coding journey. Together with some example scripts, the pptx will
form the basis of my presentation during the Coding Club session 2024-04-08.

I hope you will find it useful!

Best,  

Carl Brunius <carl.brunius@chalmers.se>  
Associate Professor Computational Metabolomics  
Department of Life Sciences  
Chalmers University of Technology  
