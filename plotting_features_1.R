# Clean slate
rm(list = ls())

# specify mode
mode <- 'HP'

# Extract mode info on CHROMATOGRAPHY and POLARITY
chrom <- substring(mode, 1, 1)
chrom <- ifelse(chrom == 'H', 'HILIC', 'RP')

pol <- substring(mode, 2, 2)
pol <- ifelse(pol == 'N', 'NEG', 'POS')

# Load cluster data for the mode
features <- read.csv2(file = paste0('MetabolomicsMetaData/ProjX_', chrom, '_', pol,'_ClusterBreakdown.csv'))
features$mz <- as.numeric(features$mz)
features$rt <- as.numeric(features$rt)

# Plot features
plot(features$rt, features$mz, pch = '.', col = rgb(0, 0, 1, .75), 
     xlab = 'Retention time (min)', ylab = 'm/z', main = paste(chrom, pol))

